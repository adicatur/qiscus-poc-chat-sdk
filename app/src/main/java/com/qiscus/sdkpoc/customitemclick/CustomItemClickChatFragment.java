package com.qiscus.sdkpoc.customitemclick;

import android.os.Bundle;
import android.widget.Toast;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.ui.fragment.QiscusChatFragment;

/**
 * Created on : February 15, 2018
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class CustomItemClickChatFragment extends QiscusChatFragment {

    public static CustomItemClickChatFragment newInstance(QiscusChatRoom qiscusChatRoom) {
        CustomItemClickChatFragment fragment = new CustomItemClickChatFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CHAT_ROOM_DATA, qiscusChatRoom);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void onItemCommentClick(QiscusComment qiscusComment) {
        Toast.makeText(getActivity(), "onItemCommentClick: " + qiscusComment.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onItemCommentLongClick(QiscusComment qiscusComment) {
        Toast.makeText(getActivity(), "onItemCommentLongClick: " + qiscusComment.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
