package com.qiscus.sdkpoc.customitemclick;

import android.content.Context;
import android.content.Intent;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.ui.QiscusChatActivity;
import com.qiscus.sdk.ui.fragment.QiscusBaseChatFragment;

/**
 * Created on : February 15, 2018
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class CustomItemClickChatActivity extends QiscusChatActivity {

    public static Intent generateIntent(Context context, QiscusChatRoom qiscusChatRoom) {
        Intent intent = new Intent(context, CustomItemClickChatActivity.class);
        intent.putExtra(CHAT_ROOM_DATA, qiscusChatRoom);
        return intent;
    }

    @Override
    protected QiscusBaseChatFragment onCreateChatFragment() {
        return CustomItemClickChatFragment.newInstance(qiscusChatRoom);
    }
}