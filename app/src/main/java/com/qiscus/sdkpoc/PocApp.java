package com.qiscus.sdkpoc;

import android.app.Application;

import com.qiscus.sdk.Qiscus;

/**
 * Created on : July 18, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class PocApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Qiscus.init(this, "sdksample");
    }
}
