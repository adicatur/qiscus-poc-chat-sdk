package com.qiscus.sdkpoc.customitemview;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.ui.adapter.OnItemClickListener;
import com.qiscus.sdk.ui.adapter.OnLongItemClickListener;
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseTextMessageViewHolder;
import com.qiscus.sdkpoc.R;

/**
 * Created on : July 18, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class CustomTextMessageViewHolder extends QiscusBaseTextMessageViewHolder {

    private QiscusComment qiscusComment;

    public CustomTextMessageViewHolder(View itemView, OnItemClickListener itemClickListener,
                                       OnLongItemClickListener longItemClickListener) {
        super(itemView, itemClickListener, longItemClickListener);
    }

    @NonNull
    @Override
    protected TextView getMessageTextView(View itemView) {
        return (TextView) itemView.findViewById(R.id.contents);
    }

    @Nullable
    @Override
    protected ImageView getFirstMessageBubbleIndicatorView(View itemView) {
        return (ImageView) itemView.findViewById(R.id.bubble);
    }

    @NonNull
    @Override
    protected View getMessageBubbleView(View itemView) {
        return itemView.findViewById(R.id.message);
    }

    @Nullable
    @Override
    protected TextView getDateView(View itemView) {
        return (TextView) itemView.findViewById(R.id.date);
    }

    @Nullable
    @Override
    protected TextView getTimeView(View itemView) {
        return (TextView) itemView.findViewById(R.id.time);
    }

    @Nullable
    @Override
    protected ImageView getMessageStateIndicatorView(View itemView) {
        return (ImageView) itemView.findViewById(R.id.icon_read);
    }

    @Nullable
    @Override
    protected ImageView getAvatarView(View itemView) {
        return (ImageView) itemView.findViewById(R.id.avatar);
    }

    @Nullable
    @Override
    protected TextView getSenderNameView(View itemView) {
        return (TextView) itemView.findViewById(R.id.name);
    }

    @Override
    protected void showMessage(QiscusComment qiscusComment) {
        this.qiscusComment = qiscusComment;
        this.messageTextView.setText(qiscusComment.getMessage());
    }

    @Override
    protected void setUpColor() {
        if (qiscusComment == null) {
            return;
        }

        if (messageFromMe) {
            messageBubbleView.setBackgroundColor(rightBubbleColor);
            if (firstMessageBubbleIndicatorView != null) {
                firstMessageBubbleIndicatorView.setColorFilter(rightBubbleColor);
            }

        } else {
            try {
                //change bubble color when metadata, sender_type is teacher, otherwise will be default bubble
                if (qiscusComment.getExtras().getString("sender_type").equals("teacher")) {
                    messageBubbleView.setBackgroundColor(ContextCompat.getColor(Qiscus.getApps().getApplicationContext(), R.color.colorAccent));
                    if (firstMessageBubbleIndicatorView != null) {
                        firstMessageBubbleIndicatorView.setColorFilter(ContextCompat.getColor(Qiscus.getApps().getApplicationContext(), R.color.colorAccent));
                    }
                } else {
                    messageBubbleView.setBackgroundColor(leftBubbleColor);
                    if (firstMessageBubbleIndicatorView != null) {
                        firstMessageBubbleIndicatorView.setColorFilter(leftBubbleColor);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (dateView != null) {
            dateView.setTextColor(dateColor);
        }

    }
}
