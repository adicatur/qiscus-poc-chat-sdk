package com.qiscus.sdkpoc.customitemview;

import android.os.Bundle;


import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter;
import com.qiscus.sdk.ui.fragment.QiscusChatFragment;

/**
 * Created on : July 18, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class CustomChatFragment extends QiscusChatFragment {

    public static CustomChatFragment newInstance(QiscusChatRoom qiscusChatRoom) {
        CustomChatFragment fragment = new CustomChatFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CHAT_ROOM_DATA, qiscusChatRoom);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected QiscusChatAdapter onCreateChatAdapter() {
        return new CustomChatAdapter(getActivity(), qiscusChatRoom.isGroup());
    }
}
