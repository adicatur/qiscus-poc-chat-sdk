package com.qiscus.sdkpoc.customitemview;

import android.content.Context;
import android.view.ViewGroup;

import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter;
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseMessageViewHolder;
import com.qiscus.sdkpoc.R;

import org.json.JSONException;

/**
 * Created on : July 18, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class CustomChatAdapter extends QiscusChatAdapter {
    private final int TYPE_MESSAGE_ME = 2333;
    private final int TYPE_MESSAGE_OTHER = 2334;
    private final int TYPE_MESSAGE_EMPTY = 2335;
    private final int TYPE_MESSAGE_METDATA = 2336;

    public CustomChatAdapter(Context context, boolean groupChat) {
        super(context, groupChat);
    }

    @Override
    protected int getItemViewTypeMyMessage(QiscusComment qiscusComment, int position) {
        if (qiscusComment.getMessage().equalsIgnoreCase("EMPTY")) {
            return TYPE_MESSAGE_EMPTY;
        } else if (qiscusComment.getType() == QiscusComment.Type.TEXT) {
            return TYPE_MESSAGE_ME;
        }
        return super.getItemViewTypeMyMessage(qiscusComment, position);
    }

    @Override
    protected int getItemViewTypeOthersMessage(QiscusComment qiscusComment, int position) {
        if (qiscusComment.getMessage().equalsIgnoreCase("EMPTY")) {
            return TYPE_MESSAGE_EMPTY;
        } else if (qiscusComment.getType() == QiscusComment.Type.TEXT) {
            return TYPE_MESSAGE_OTHER;
        } else {
            try {
                if (qiscusComment.getType() == QiscusComment.Type.TEXT &&
                        qiscusComment.getExtras().getString("sender_type").equals("teacher")) {
                    return TYPE_MESSAGE_METDATA;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return super.getItemViewTypeOthersMessage(qiscusComment, position);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        switch (viewType) {
            case TYPE_MESSAGE_ME:
                return R.layout.item_message_text_me;
            case TYPE_MESSAGE_OTHER:
                return R.layout.item_message_text;
            case TYPE_MESSAGE_EMPTY:
                return R.layout.item_message_empty_system;
            case TYPE_MESSAGE_METDATA:
                return R.layout.item_message_text_bubble;
            default:
                return super.getItemResourceLayout(viewType);
        }
    }

    @Override
    public QiscusBaseMessageViewHolder<QiscusComment> onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_MESSAGE_ME:
            case TYPE_MESSAGE_OTHER:
                return new CustomTextMessageViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            case TYPE_MESSAGE_EMPTY:
                return new EmptySystemEventViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            case TYPE_MESSAGE_METDATA:
                return new CustomTextMessageViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }
}
