package com.qiscus.sdkpoc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;
import com.qiscus.sdkpoc.customcomment.StickerChatActivity;
import com.qiscus.sdkpoc.custominput.CustomInputChatActivity;
import com.qiscus.sdkpoc.customitemclick.CustomItemClickChatActivity;
import com.qiscus.sdkpoc.customitemview.CustomChatActivity;
import com.qiscus.sdkpoc.listroom.RoomsActivity;
import com.qiscus.sdkpoc.viewpager.ViewPagerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;
    private Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLoginButton = findViewById(R.id.bt_login);
        mLoginButton.setText(Qiscus.hasSetupUser() ? "Logout" : "Login");
    }

    public void loginOrLogout(View view) {
        if (Qiscus.hasSetupUser()) {
            Qiscus.clearUser();
            mLoginButton.setText("Login");
        } else {
            showLoading();
            Qiscus.setUser("m@mail.com", "12345678")
                    .withUsername("Marco")
                    .save()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(qiscusAccount -> {
                        Log.i("MainActivity", "Login with account: " + qiscusAccount);
                        mLoginButton.setText("Logout");
                        dismissLoading();
                    }, throwable -> {
                        throwable.printStackTrace();
                        showError(throwable.getMessage());
                        dismissLoading();
                    });
        }
    }

    public void listRoom(View view) {
        startActivity(new Intent(this, RoomsActivity.class));
    }

    public void customItemView(View view) {
        showLoading();
        Qiscus.buildChatRoomWith("l@mail.com")
                .build()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(qiscusChatRoom -> CustomChatActivity.generateIntent(this, qiscusChatRoom))
                .subscribe(intent -> {
                    revertStickerChatConfig();
                    startActivity(intent);
                    dismissLoading();
                }, throwable -> {
                    throwable.printStackTrace();
                    showError(throwable.getMessage());
                    dismissLoading();
                });
    }

    public void customStickerType(View view) {
        showLoading();
        Qiscus.buildChatRoomWith("l@mail.com")
                .build()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(qiscusChatRoom -> StickerChatActivity.generateIntent(this, qiscusChatRoom))
                .subscribe(intent -> {
                    setupStickerChatConfig();
                    startActivity(intent);
                    dismissLoading();
                }, throwable -> {
                    throwable.printStackTrace();
                    showError(throwable.getMessage());
                    dismissLoading();
                });
    }

    public void sendStickerType(View view) {
        String message = "Funny Sticker";
        JSONObject payload = new JSONObject();
        try {
            payload.put("sticker_url", "https://res.cloudinary.com/qiscus/image/upload/fxwzBRPcdz/Bubble-Pup-Yup.gif");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        QiscusComment comment = QiscusComment.generateCustomMessage(1918511, message, "sticker", payload);
        sendQiscusComment(comment);
    }

    public void sendMessageWithMetadata(View view) {
        try {
            JSONObject metadataUser = new JSONObject();
            metadataUser.put("sender_type", "teacher");

            QiscusComment comment = QiscusComment.generateMessage(1918511, "Hi metadata");
            comment.setExtras(metadataUser);
            sendQiscusComment(comment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendEmptyMessage(View view) {
        String message = "Empty";
        JSONObject payload = new JSONObject();
        try {
            payload.put("empty", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        QiscusComment comment = QiscusComment.generateCustomMessage(1918511, message, "empty", payload);
        sendQiscusComment(comment);
    }

    private void sendQiscusComment(QiscusComment qiscusComment) {
        QiscusApi.getInstance().postComment(qiscusComment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(commentSend -> {
                    //Success
                }, throwable -> {
                    //Error
                });
    }

    public void customInputForm(View view) {
        showLoading();
        Qiscus.buildChatRoomWith("l@mail.com")
                .build()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(qiscusChatRoom -> CustomInputChatActivity.generateIntent(this, qiscusChatRoom))
                .subscribe(intent -> {
                    setupStickerChatConfig();
                    startActivity(intent);
                    dismissLoading();
                }, throwable -> {
                    throwable.printStackTrace();
                    showError(throwable.getMessage());
                    dismissLoading();
                });
    }

    public void openViewPager(View view) {
        showLoading();
        Qiscus.buildChatRoomWith("l@mail.com")
                .build()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(qiscusChatRoom -> ViewPagerActivity.generateIntent(this, qiscusChatRoom))
                .subscribe(intent -> {
                    revertStickerChatConfig();
                    startActivity(intent);
                    dismissLoading();
                }, throwable -> {
                    throwable.printStackTrace();
                    showError(throwable.getMessage());
                    dismissLoading();
                });
    }

    public void customItemClick(View view) {
        showLoading();
        Qiscus.buildChatRoomWith("l@mail.com")
                .build()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(qiscusChatRoom -> CustomItemClickChatActivity.generateIntent(this, qiscusChatRoom))
                .subscribe(intent -> {
                    revertStickerChatConfig();
                    startActivity(intent);
                    dismissLoading();
                }, throwable -> {
                    throwable.printStackTrace();
                    showError(throwable.getMessage());
                    dismissLoading();
                });
    }

    private void revertStickerChatConfig() {
        Qiscus.getChatConfig()
                .setSendButtonIcon(R.drawable.ic_qiscus_send)
                .setShowAttachmentPanelIcon(R.drawable.ic_qiscus_attach);
    }

    private void setupStickerChatConfig() {
        Qiscus.getChatConfig()
                .setSendButtonIcon(R.drawable.ic_qiscus_send_on)
                .setShowAttachmentPanelIcon(R.drawable.ic_qiscus_send_off);
    }

    public void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    public void showLoading() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please wait...");
        }
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissLoading() {
        mProgressDialog.dismiss();
    }
}
