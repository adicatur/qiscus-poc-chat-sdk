package com.qiscus.sdkpoc.listroom;

import android.content.Context;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qiscus.sdk.Qiscus;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.model.QiscusRoomMember;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;
import com.qiscus.sdk.chat.core.data.remote.QiscusPusherApi;
import com.qiscus.sdk.chat.core.event.QiscusChatRoomEvent;
import com.qiscus.sdk.chat.core.event.QiscusCommentReceivedEvent;
import com.qiscus.sdkpoc.R;
import com.qiscus.sdkpoc.util.ItemClickListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on : October 13, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.VH> {

    private Context context;
    private SortedList<QiscusChatRoom> data;
    private TypingHolder typingHolder;
    private NewCommentHolder newCommentHolder;

    private ItemClickListener itemClickListener;

    public RoomAdapter(Context context) {
        this.context = context;
        typingHolder = new TypingHolder();
        newCommentHolder = new NewCommentHolder();
        EventBus.getDefault().register(this);

        data = new SortedList<>(QiscusChatRoom.class, new SortedList.Callback<QiscusChatRoom>() {
            @Override
            public int compare(QiscusChatRoom o1, QiscusChatRoom o2) {
                return o2.getLastComment().getTime().compareTo(o1.getLastComment().getTime());
            }

            @Override
            public void onChanged(int position, int count) {

            }

            @Override
            public boolean areContentsTheSame(QiscusChatRoom oldItem, QiscusChatRoom newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(QiscusChatRoom item1, QiscusChatRoom item2) {
                return item1.equals(item2);
            }

            @Override
            public void onInserted(int position, int count) {

            }

            @Override
            public void onRemoved(int position, int count) {

            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
                notifyItemChanged(toPosition);
            }
        });

    }

    public void addOrUpdateRoom(List<QiscusChatRoom> qiscusChatRooms) {
        for (QiscusChatRoom qiscusChatRoom : qiscusChatRooms) {
            int i = findPosition(qiscusChatRoom);
            if (i >= 0) {
                data.updateItemAt(i, qiscusChatRoom);
            } else {
                data.add(qiscusChatRoom);
            }
        }
        notifyDataSetChanged();
    }

    private void add(QiscusChatRoom qiscusChatRoom) {
        int i = data.add(qiscusChatRoom);
        notifyItemChanged(i);
    }

    private void addOrUpdate(QiscusChatRoom qiscusChatRoom) {
        int i = findPosition(qiscusChatRoom);
        if (i >= 0) {
            data.updateItemAt(i, qiscusChatRoom);
            notifyItemChanged(i);
        } else {
            add(qiscusChatRoom);
        }
    }

    private int findPosition(QiscusChatRoom qiscusChatRoom) {
        if (qiscusChatRoom == null) {
            return -1;
        }

        int size = data.size() - 1;
        for (int i = size; i >= 0; i--) {
            if (data.get(i).equals(qiscusChatRoom)) {
                return i;
            }
        }

        return -1;
    }

    private void listenRoom(QiscusChatRoom qiscusChatRoom) {
        QiscusPusherApi.getInstance().listenRoom(qiscusChatRoom);
    }

    private void unlistenRoom(QiscusChatRoom qiscusChatRoom) {
        QiscusPusherApi.getInstance().unListenRoom(qiscusChatRoom);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public QiscusChatRoom getRoom(int position) {
        return data.get(position);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(context)
                .inflate(R.layout.item_room, parent, false), typingHolder, newCommentHolder, itemClickListener);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Subscribe
    public void onRoomEvent(QiscusChatRoomEvent event) {
        if (event.getEvent() == QiscusChatRoomEvent.Event.TYPING) {
            for (TypingListener typingListener : typingHolder.typingListeners) {
                typingListener.onUserTyping(event.getRoomId(), event.getUser(), event.isTyping());
            }
        }
    }

    @Subscribe
    public void onNewComment(QiscusCommentReceivedEvent event) {
        for (NewCommentListener newCommentListener : newCommentHolder.newCommentListeners) {
            newCommentListener.onNewComment(event.getQiscusComment());
        }
    }

    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        for (int i = 0; i < data.size(); i++) {
            unlistenRoom(data.get(i));
        }
    }

    private static class TypingHolder {
        private List<TypingListener> typingListeners = new ArrayList<>();

        private void registerTypingListener(TypingListener typingListener) {
            if (!typingListeners.contains(typingListener)) {
                typingListeners.add(typingListener);
            }
        }
    }

    interface TypingListener {
        void onUserTyping(long roomId, String userEmail, boolean typing);
    }

    private static class NewCommentHolder {
        private List<NewCommentListener> newCommentListeners = new ArrayList<>();

        private void registerCommentListener(NewCommentListener commentListener) {
            if (!newCommentListeners.contains(commentListener)) {
                newCommentListeners.add(commentListener);
            }
        }
    }

    interface NewCommentListener {
        void onNewComment(QiscusComment comment);
    }

    public class VH extends RecyclerView.ViewHolder implements TypingListener, NewCommentListener {
        private TextView roomName;
        private TextView lastMessage;
        private TextView unreadCount;

        private QiscusChatRoom room;

        VH(View itemView, TypingHolder typingHolder, NewCommentHolder newCommentHolder, ItemClickListener itemClickListener) {
            super(itemView);
            roomName = itemView.findViewById(R.id.room_name);
            lastMessage = itemView.findViewById(R.id.last_message);
            unreadCount = itemView.findViewById(R.id.unread_count);
            typingHolder.registerTypingListener(this);
            newCommentHolder.registerCommentListener(this);
            itemView.setOnClickListener(v -> {
                if (itemClickListener != null) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }

        void bind(QiscusChatRoom room) {
            this.room = room;
            roomName.setText(room.getName());
            lastMessage.setText(room.getLastComment().getMessage());
            unreadCount.setText(String.format("%d", room.getUnreadCount()));
            unreadCount.setVisibility(room.getUnreadCount() > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onUserTyping(long roomId, String userEmail, boolean typing) {
            if (room.getId() == roomId) {
                if (!typing) {
                    lastMessage.setText(room.getLastComment().getMessage());
                    return;
                }

                for (QiscusRoomMember member : room.getMember()) {
                    if (member.getEmail().equals(userEmail)) {
                        lastMessage.setText(String.format("%s is typing...", member.getUsername().split(" ")[0]));
                        break;
                    }
                }
            }
        }

        @Override
        public void onNewComment(QiscusComment comment) {
            if (comment.getRoomId() == room.getId() && !comment.getUniqueId().equals(room.getLastComment().getUniqueId())) {
                room.setLastComment(comment);

                if (comment.getSenderEmail().equals(Qiscus.getQiscusAccount().getEmail())) {
                    room.setUnreadCount(0);
                } else {
                    room.setUnreadCount(room.getUnreadCount() + 1);
                }

                updateChatRoom (comment);

                lastMessage.setText(room.getLastComment().getMessage());
                unreadCount.setText(String.format("%d", room.getUnreadCount()));
                unreadCount.setVisibility(room.getUnreadCount() > 0 ? View.VISIBLE : View.GONE);
            }
        }

        private void updateChatRoom(QiscusComment qiscusComment) {
            QiscusChatRoom savedQiscusChatRoom = Qiscus.getDataStore().getChatRoom(qiscusComment.getRoomId());

            if (savedQiscusChatRoom != null) {
                addOrUpdate(savedQiscusChatRoom);
            } else {
                QiscusApi.getInstance().getChatRoom(qiscusComment.getRoomId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(RoomAdapter.this::addOrUpdate,
                                Throwable::printStackTrace);
            }
        }
    }


}
