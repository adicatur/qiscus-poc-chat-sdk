package com.qiscus.sdkpoc.listroom;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;
import com.qiscus.sdk.chat.core.util.QiscusRxExecutor;
import com.qiscus.sdk.ui.QiscusChatActivity;
import com.qiscus.sdk.ui.QiscusGroupChatActivity;
import com.qiscus.sdkpoc.R;

import java.util.List;

/**
 * Created on : October 13, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class RoomsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RoomAdapter roomAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        roomAdapter = new RoomAdapter(this);
        recyclerView.setAdapter(roomAdapter);

        roomAdapter.setItemClickListener(position -> {
            //Just get the room instance, no need to request api or local db
            QiscusChatRoom room = roomAdapter.getRoom(position);
            if (room.isGroup()) {
                startActivity(QiscusGroupChatActivity.generateIntent(this, room));
            } else {
                startActivity(QiscusChatActivity.generateIntent(this, room));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadRooms();
    }

    private void loadRooms() {
        QiscusRxExecutor.execute(QiscusApi.getInstance().getChatRooms(1, 100, true), new QiscusRxExecutor.Listener<List<QiscusChatRoom>>() {
            @Override
            public void onSuccess(List<QiscusChatRoom> result) {
                roomAdapter.addOrUpdateRoom(result);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        roomAdapter.onDestroy();
    }
}
