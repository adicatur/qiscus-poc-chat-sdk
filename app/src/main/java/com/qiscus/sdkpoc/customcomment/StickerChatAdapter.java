package com.qiscus.sdkpoc.customcomment;

import android.content.Context;
import android.view.ViewGroup;

import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter;
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseMessageViewHolder;
import com.qiscus.sdkpoc.R;
import com.qiscus.sdkpoc.customitemview.CustomTextMessageViewHolder;
import com.qiscus.sdkpoc.customitemview.EmptySystemEventViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created on : August 23, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class StickerChatAdapter extends QiscusChatAdapter {
    private static final int TYPE_STICKER_ME = 23323;
    private static final int TYPE_STICKER_OTHER = 23324;
    private static final int TYPE_MESSAGE_METADATA = 23325;
    private final int TYPE_MESSAGE_EMPTY = 2335;

    public StickerChatAdapter(Context context, boolean groupChat) {
        super(context, groupChat);
    }

    @Override
    protected int getItemViewTypeCustomMessage(QiscusComment qiscusComment, int position) {
        try {
            JSONObject payload = new JSONObject(qiscusComment.getExtraPayload());
            if (payload.optString("type").equals("sticker")) {
                return qiscusComment.getSenderEmail().equals(qiscusAccount.getEmail()) ? TYPE_STICKER_ME : TYPE_STICKER_OTHER;
            }

            if (payload.optString("type").equals("empty")) {
                return TYPE_MESSAGE_EMPTY;
            }
        } catch (JSONException ignored) {

        }
        return super.getItemViewTypeCustomMessage(qiscusComment, position);
    }

    @Override
    protected int getItemViewTypeOthersMessage(QiscusComment qiscusComment, int position) {
        try {
            if (qiscusComment.getType() == QiscusComment.Type.TEXT &&
                    qiscusComment.getExtras().getString("sender_type").equals("teacher")) {
                return TYPE_MESSAGE_METADATA;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return super.getItemViewTypeOthersMessage(qiscusComment, position);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        switch (viewType) {
            case TYPE_STICKER_ME:
                return R.layout.item_message_sticker_me;
            case TYPE_STICKER_OTHER:
                return R.layout.item_message_sticker;
            case TYPE_MESSAGE_EMPTY:
                return R.layout.item_message_empty_system;
            case TYPE_MESSAGE_METADATA:
                return R.layout.item_message_text_bubble;
            default:
                return super.getItemResourceLayout(viewType);
        }
    }

    @Override
    public QiscusBaseMessageViewHolder<QiscusComment> onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_STICKER_ME:
            case TYPE_STICKER_OTHER:
                return new StickerMessageViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            case TYPE_MESSAGE_EMPTY:
                return new EmptySystemEventViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            case TYPE_MESSAGE_METADATA:
                return new CustomTextMessageViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }
}
