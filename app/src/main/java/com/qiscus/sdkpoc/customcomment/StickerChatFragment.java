package com.qiscus.sdkpoc.customcomment;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.model.QiscusRoomMember;
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter;
import com.qiscus.sdk.ui.fragment.QiscusBaseChatFragment;
import com.qiscus.sdk.ui.fragment.QiscusChatFragment;
import com.qiscus.sdk.ui.view.QiscusAudioRecorderView;
import com.qiscus.sdk.ui.view.QiscusMentionSuggestionView;
import com.qiscus.sdk.ui.view.QiscusRecyclerView;
import com.qiscus.sdk.ui.view.QiscusReplyPreviewView;
import com.qiscus.sdk.util.QiscusSpannableBuilder;
import com.qiscus.sdkpoc.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on : August 23, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class StickerChatFragment extends QiscusBaseChatFragment implements ActionMode.Callback {
    private ImageView mAttachButton;
    private LinearLayout mAddPanel;

    protected QiscusChatFragment.UserTypingListener userTypingListener;
    private ActionMode actionMode;
    private Map<String, QiscusRoomMember> roomMembers;

    public static StickerChatFragment newInstance(QiscusChatRoom qiscusChatRoom) {
        StickerChatFragment fragment = new StickerChatFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CHAT_ROOM_DATA, qiscusChatRoom);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_sticker_chat;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (activity instanceof QiscusChatFragment.UserTypingListener) {
            userTypingListener = (QiscusChatFragment.UserTypingListener) activity;
        }
    }

    @Override
    protected void toggleSelectComment(QiscusComment qiscusComment) {
        super.toggleSelectComment(qiscusComment);
        onCommentSelected(chatAdapter.getSelectedComments());
    }

    @Override
    protected void onLoadView(View view) {
        super.onLoadView(view);
        mAttachButton = view.findViewById(R.id.button_attach);
        mAddPanel = view.findViewById(R.id.add_panel);
        mAttachButton.setOnClickListener(v -> {
            if (mAddPanel.getVisibility() == View.GONE) {
                mAddPanel.startAnimation(animation);
                mAddPanel.setVisibility(View.VISIBLE);
            } else {
                mAddPanel.setVisibility(View.GONE);
            }
        });

        //Simulate send sticker
        if (toggleEmojiButton != null) {
            toggleEmojiButton.setOnLongClickListener(v -> {
                sendSticker();
                return true;
            });
        }
    }

    private void sendSticker() {
        String message = "Funny Sticker";
        JSONObject payload = new JSONObject();
        try {
            payload.put("sticker_url", "https://res.cloudinary.com/qiscus/image/upload/fxwzBRPcdz/Bubble-Pup-Yup.gif");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        QiscusComment comment = QiscusComment.generateCustomMessage(qiscusChatRoom.getId(), message, "sticker", payload);
        sendQiscusComment(comment);
    }

    @Override
    protected void onApplyChatConfig() {
        super.onApplyChatConfig();
        if (addImageButton != null) {
            addImageButton.setBackground(ContextCompat.getDrawable(Qiscus.getApps(),
                    R.drawable.bt_qiscus_selector_grey));
        }
        if (takeImageButton != null) {
            takeImageButton.setBackground(ContextCompat.getDrawable(Qiscus.getApps(),
                    R.drawable.bt_qiscus_selector_grey));
        }
        if (addFileButton != null) {
            addFileButton.setBackground(ContextCompat.getDrawable(Qiscus.getApps(),
                    R.drawable.bt_qiscus_selector_grey));
        }
        if (recordAudioButton != null) {
            recordAudioButton.setBackground(ContextCompat.getDrawable(Qiscus.getApps(),
                    R.drawable.bt_qiscus_selector_grey));
        }
    }

    @NonNull
    @Override
    protected ViewGroup getRootView(View view) {
        return (ViewGroup) view.findViewById(R.id.root_view);
    }

    @Nullable
    @Override
    protected ViewGroup getEmptyChatHolder(View view) {
        return (ViewGroup) view.findViewById(R.id.empty_chat);
    }

    @NonNull
    @Override
    protected SwipeRefreshLayout getSwipeRefreshLayout(View view) {
        return (SwipeRefreshLayout) view.findViewById(R.id.swipe_layout);
    }

    @NonNull
    @Override
    protected QiscusRecyclerView getMessageRecyclerView(View view) {
        return (QiscusRecyclerView) view.findViewById(R.id.list_message);
    }

    @Nullable
    @Override
    protected ViewGroup getMessageInputPanel(View view) {
        return (ViewGroup) view.findViewById(R.id.input_panel);
    }

    @Nullable
    @Override
    protected ViewGroup getMessageEditTextContainer(View view) {
        return null;
    }

    @NonNull
    @Override
    protected EditText getMessageEditText(View view) {
        return (EditText) view.findViewById(R.id.field_message);
    }

    @NonNull
    @Override
    protected ImageView getSendButton(View view) {
        return (ImageView) view.findViewById(R.id.button_send);
    }

    @NonNull
    @Override
    protected QiscusMentionSuggestionView getMentionSuggestionView(View view) {
        return null;
    }

    @Nullable
    @Override
    protected View getNewMessageButton(View view) {
        return view.findViewById(R.id.button_new_message);
    }

    @NonNull
    @Override
    protected View getLoadMoreProgressBar(View view) {
        return view.findViewById(R.id.progressBar);
    }

    @Nullable
    @Override
    protected ImageView getEmptyChatImageView(View view) {
        return (ImageView) view.findViewById(R.id.empty_chat_icon);
    }

    @Nullable
    @Override
    protected TextView getEmptyChatTitleView(View view) {
        return (TextView) view.findViewById(R.id.empty_chat_title);
    }

    @Nullable
    @Override
    protected TextView getEmptyChatDescView(View view) {
        return (TextView) view.findViewById(R.id.empty_chat_desc);
    }

    @Nullable
    @Override
    protected ViewGroup getAttachmentPanel(View view) {
        return null;
    }

    @Nullable
    @Override
    protected View getAddImageLayout(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getAddImageButton(View view) {
        return (ImageView) view.findViewById(R.id.button_add_image);
    }

    @Nullable
    @Override
    protected View getTakeImageLayout(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getTakeImageButton(View view) {
        return (ImageView) view.findViewById(R.id.button_pick_picture);
    }

    @Nullable
    @Override
    protected View getAddFileLayout(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getAddFileButton(View view) {
        return (ImageView) view.findViewById(R.id.button_add_file);
    }

    @Nullable
    @Override
    protected View getRecordAudioLayout(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getRecordAudioButton(View view) {
        return (ImageView) view.findViewById(R.id.button_add_audio);
    }

    @Nullable
    @Override
    protected View getAddContactLayout(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getAddContactButton(View view) {
        return null;
    }

    @Nullable
    @Override
    protected View getAddLocationLayout(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getAddLocationButton(View view) {
        return null;
    }

    @Nullable
    @Override
    public ImageView getHideAttachmentButton(View view) {
        return null;
    }

    @Nullable
    @Override
    protected ImageView getToggleEmojiButton(View view) {
        return (ImageView) view.findViewById(R.id.button_emoji);
    }

    @Nullable
    @Override
    protected QiscusAudioRecorderView getRecordAudioPanel(View view) {
        return (QiscusAudioRecorderView) view.findViewById(R.id.record_panel);
    }

    @Nullable
    @Override
    protected QiscusReplyPreviewView getReplyPreviewView(View view) {
        return (QiscusReplyPreviewView) view.findViewById(R.id.reply_preview);
    }

    @Nullable
    @Override
    protected View getGotoBottomButton(View view) {
        return null;
    }

    @Override
    protected QiscusChatAdapter onCreateChatAdapter() {
        return new StickerChatAdapter(getActivity(), qiscusChatRoom.isGroup());
    }

    @Override
    public void onUserTyping(String user, boolean typing) {
        if (userTypingListener != null) {
            userTypingListener.onUserTyping(user, typing);
        }
    }

    protected void recordAudio() {
        super.recordAudio();
        mAddPanel.setVisibility(View.GONE);
    }

    private void onCommentSelected(List<QiscusComment> selectedComments) {
        boolean hasCheckedItems = selectedComments.size() > 0;
        if (hasCheckedItems && actionMode == null) {
            actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this);
        } else if (!hasCheckedItems && actionMode != null) {
            actionMode.finish();
        }

        if (actionMode != null) {
            actionMode.setTitle(getString(com.qiscus.sdk.R.string.qiscus_selected_comment, selectedComments.size()));
            if (selectedComments.size() == 1 && selectedComments.get(0).getState() >= QiscusComment.STATE_ON_QISCUS) {
                actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_reply).setVisible(true);
                QiscusComment qiscusComment = selectedComments.get(0);
                File localPath = Qiscus.getDataStore().getLocalPath(qiscusComment.getId());
                if (localPath != null) {
                    actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_share).setVisible(true);
                } else {
                    actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_share).setVisible(false);
                }
            } else {
                actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_reply).setVisible(false);
                actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_share).setVisible(false);
            }

            if (onlyTextOrLinkType(selectedComments)) {
                actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_copy).setVisible(true);
            } else {
                actionMode.getMenu().findItem(com.qiscus.sdk.R.id.action_copy).setVisible(false);
            }
        }
    }

    private boolean onlyTextOrLinkType(List<QiscusComment> selectedComments) {
        for (QiscusComment selectedComment : selectedComments) {
            if (selectedComment.getType() != QiscusComment.Type.TEXT
                    && selectedComment.getType() != QiscusComment.Type.LINK
                    && selectedComment.getType() != QiscusComment.Type.REPLY
                    && selectedComment.getType() != QiscusComment.Type.CONTACT
                    && selectedComment.getType() != QiscusComment.Type.LOCATION) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(com.qiscus.sdk.R.menu.qiscus_comment_action, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        menu.findItem(com.qiscus.sdk.R.id.action_reply).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(com.qiscus.sdk.R.id.action_copy).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(com.qiscus.sdk.R.id.action_share).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        onSelectedCommentsAction(mode, item, getSelectedComments());
        return false;
    }

    protected void onSelectedCommentsAction(ActionMode mode, MenuItem item, List<QiscusComment> selectedComments) {
        int i = item.getItemId();
        if (i == com.qiscus.sdk.R.id.action_copy) {
            if (roomMembers == null) {
                roomMembers = new HashMap<>();
                for (QiscusRoomMember member : qiscusChatRoom.getMember()) {
                    roomMembers.put(member.getEmail(), member);
                }
            }
            String text = "";
            if (selectedComments.size() == 1) {
                QiscusComment qiscusComment = selectedComments.get(0);
                text = qiscusComment.isAttachment() ? qiscusComment.getAttachmentName() :
                        new QiscusSpannableBuilder(qiscusComment.getMessage(), roomMembers)
                                .build().toString();
            } else {
                for (QiscusComment qiscusComment : selectedComments) {
                    text += qiscusComment.getSender() + ": ";
                    text += qiscusComment.isAttachment() ? qiscusComment.getAttachmentName() :
                            new QiscusSpannableBuilder(qiscusComment.getMessage(), roomMembers)
                                    .build().toString();
                    text += "\n";
                }
            }

            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getString(com.qiscus.sdk.R.string.qiscus_chat_activity_label_clipboard), text);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(getContext(), getString(com.qiscus.sdk.R.string.qiscus_copied_message, selectedComments.size()), Toast.LENGTH_SHORT).show();
        } else if (i == com.qiscus.sdk.R.id.action_share) {
            if (selectedComments.size() > 0) {
                QiscusComment qiscusComment = selectedComments.get(0);
                String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(qiscusComment.getExtension());
                File file = Qiscus.getDataStore().getLocalPath(qiscusComment.getId());
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType(mime);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                } else {
                    intent.putExtra(Intent.EXTRA_STREAM,
                            FileProvider.getUriForFile(getContext(), Qiscus.getProviderAuthorities(), file));
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivity(Intent.createChooser(intent, getString(com.qiscus.sdk.R.string.qiscus_share_image_title)));
            }
        } else if (i == com.qiscus.sdk.R.id.action_reply) {
            if (selectedComments.size() > 0) {
                replyComment(selectedComments.get(0));
            }
        }
        mode.finish();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        clearSelectedComments();
    }
}
