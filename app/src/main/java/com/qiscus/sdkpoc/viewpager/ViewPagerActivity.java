package com.qiscus.sdkpoc.viewpager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.Qiscus;

import com.qiscus.sdk.chat.core.data.model.QiscusAccount;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusRoomMember;
import com.qiscus.sdk.chat.core.util.QiscusDateUtil;
import com.qiscus.sdk.presenter.QiscusUserStatusPresenter;
import com.qiscus.sdk.ui.fragment.QiscusBaseChatFragment;
import com.qiscus.sdk.ui.fragment.QiscusChatFragment;
import com.qiscus.sdk.ui.view.QiscusCircularImageView;
import com.qiscus.sdkpoc.R;

import java.util.Date;

/**
 * Created on : October 16, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class ViewPagerActivity extends AppCompatActivity implements QiscusBaseChatFragment.RoomChangedListener,
        QiscusChatFragment.UserTypingListener, QiscusUserStatusPresenter.View {
    private static final String CHAT_ROOM_DATA = "chat_room_data";

    protected Toolbar toolbar;
    protected TextView tvTitle;
    protected TextView tvSubtitle;
    protected QiscusCircularImageView ivAvatar;

    private QiscusChatRoom qiscusChatRoom;
    private QiscusUserStatusPresenter userStatusPresenter;

    protected QiscusAccount qiscusAccount;

    public static Intent generateIntent(Context context, QiscusChatRoom qiscusChatRoom) {
        Intent intent = new Intent(context, ViewPagerActivity.class);
        intent.putExtra(CHAT_ROOM_DATA, qiscusChatRoom);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        resolveChatRoom(savedInstanceState);

        qiscusAccount = Qiscus.getQiscusAccount();

        userStatusPresenter = new QiscusUserStatusPresenter(this);

        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), qiscusChatRoom));

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        toolbar = findViewById(R.id.toolbar);
        tvTitle = findViewById(R.id.tv_title);
        tvSubtitle = findViewById(R.id.tv_subtitle);
        ivAvatar = findViewById(R.id.profile_picture);
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        setSupportActionBar(toolbar);

        bindRoom();
    }

    protected void resolveChatRoom(Bundle savedInstanceState) {
        qiscusChatRoom = getIntent().getParcelableExtra(CHAT_ROOM_DATA);
        if (qiscusChatRoom == null && savedInstanceState != null) {
            qiscusChatRoom = savedInstanceState.getParcelable(CHAT_ROOM_DATA);
        }

        if (qiscusChatRoom == null) {
            finish();
        }
    }

    @Override
    public void onUserStatusChanged(String user, boolean online, Date lastActive) {
        String last = QiscusDateUtil.getRelativeTimeDiff(lastActive);
        tvSubtitle.setText(online ? getString(R.string.qiscus_online) : getString(R.string.qiscus_last_seen, last));
        tvSubtitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRoomUpdated(QiscusChatRoom qiscusChatRoom) {
        this.qiscusChatRoom = qiscusChatRoom;
        bindRoom();
        listenUser();
    }

    private void bindRoom() {
        tvTitle.setText(qiscusChatRoom.getName());
        showRoomImage();
    }

    private void listenUser() {
        for (QiscusRoomMember member : qiscusChatRoom.getMember()) {
            if (!member.getEmail().equals(Qiscus.getQiscusAccount().getEmail())) {
                userStatusPresenter.listenUser(member.getEmail());
            }
        }
    }

    protected void showRoomImage() {
        for (QiscusRoomMember member : qiscusChatRoom.getMember()) {
            if (!member.getEmail().equalsIgnoreCase(qiscusAccount.getEmail())) {
                Nirmana.getInstance().get().load(member.getAvatar())
//                        .error(R.int.ic_qiscus_avatar)
//                        .placeholder(R.drawable.ic_qiscus_avatar)
//                        .dontAnimate()
                        .into(ivAvatar);
                break;
            }
        }
    }


    @Override
    public void onUserTyping(String user, boolean typing) {
        tvSubtitle.setText(typing ? getString(R.string.qiscus_typing) : getString(R.string.qiscus_online));
        tvSubtitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String errorMessage) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userStatusPresenter.detachView();
    }
}
