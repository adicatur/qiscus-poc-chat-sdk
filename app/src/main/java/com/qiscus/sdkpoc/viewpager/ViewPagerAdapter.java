package com.qiscus.sdkpoc.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdkpoc.customcomment.StickerChatFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on : October 16, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragments = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm, QiscusChatRoom chatRoom) {
        super(fm);
        fragments.add(StickerChatFragment.newInstance(chatRoom));
        fragments.add(new EmptyFragment());
        fragments.add(new EmptyFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Fragment " + (position + 1);
    }
}
