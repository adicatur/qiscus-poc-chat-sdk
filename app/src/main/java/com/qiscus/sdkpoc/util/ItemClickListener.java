package com.qiscus.sdkpoc.util;

/**
 * Created on : October 16, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public interface ItemClickListener {
    void onClick(int position);
}
